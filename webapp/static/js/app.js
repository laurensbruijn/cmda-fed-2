var movieApp = movieApp || {};

(function() {

  movieApp.controller = {
    searchActive: false,
    filterActive: false,
    activePage: "",
    filteredList: [],

    init: function() {
      movieApp.sections.init();
      movieApp.router.init();
      this.handlers();
    },
    handlers: function() {
      var search = document.getElementById("search");
      var group = document.getElementById("group");
      var sort = document.getElementById("sort");
      var searchValue = document.getElementById("search-value");
      var closeButton = document.getElementById("close");
      var swipeNav = new Hammer(document.querySelector("body"));

      search.addEventListener("submit",function(e) {
        movieApp.utils.search(searchValue.value);
      }, false);

      swipeNav.on("swipeleft swiperight", function(ev) {
          if (ev.type == "swipeleft") {
            movieApp.sections.toggle('movies');
            window.location.hash = '#movies';
          }
          if (ev.type == "swiperight") {
            movieApp.sections.toggle('about');
            window.location.hash = '#about';
          }
      });

      sort.onclick = function() {
        movieApp.utils.sort();
        if(document.querySelector(".active-filter")) {
          document.querySelector(".active-filter").classList.remove('active-filter');
        }
        this.classList.add('active-filter');
        return false;
      }

      group.onclick = function(e) {
        movieApp.utils.group();
        if(document.querySelector(".active-filter")) {
          document.querySelector(".active-filter").classList.remove('active-filter');
        }
        this.classList.add('active-filter');
        return false;
      }

      closeButton.onclick = function(e) {
        document.querySelector("body").classList.remove('noscroll');
        movieApp.sections.toggle('movies');
        window.location.hash = '#movies';
        return false;
      }

      document.onkeydown = function(e) {
        e = e || window.event;
        switch(e.which || e.keyCode) {
          case 27: // ESC-key
            if (movieApp.controller.activePage == "movie") { // can not be 'this' because of event scope
              document.querySelector("body").classList.remove('noscroll');
              movieApp.sections.toggle('movies');
              window.location.hash = '#movies';
            }
            break;
          default: return; // exit handler for other keys
        }
        e.preventDefault();
      }
    }
  }

  movieApp.router = {
    init: function() {
      routie({
        '': function() {
          console.log("Route: default (#about)");
          movieApp.sections.toggle('about');
          movieApp.controller.activePage = "about";
        },
        'movies': function() {
          console.log("Route: #movies");
          movieApp.sections.toggle('movies');
          movieApp.controller.activePage = "movies";
        },
        'movies/:title': function(title) {
          console.log("Route: #movies/" + title);
          document.querySelector("body").classList.add('noscroll');
          movieApp.sections.toggle('movie');
          movieApp.sections.movie(title);
          movieApp.controller.activePage = "movie";
        },
        'movies/genre/:genre': function(genre) {
          console.log("Route: #movies/genre/"+ genre);
          movieApp.sections.toggle('movies');
          movieApp.utils.moviesFilter(genre);
          movieApp.controller.activePage = "moviesgenre";
        },
        'about': function() {
          console.log("Route: #about");
          movieApp.sections.toggle('about');
          movieApp.controller.activePage = "about";
        }
      });
    }
  }

  movieApp.content = {
    about: {
      title: "About this app",
      description: [
        {paragraph: "Cities fall but they are rebuilt. heroes die but they are remembered. the man likes to play chess; let's get him some rocks. circumstances have taught me that a man's ethics are the only possessions he will take beyond the grave. multiply your anger by about a hundred, kate, that's how much he thinks he loves you. bruce... i'm god. multiply your anger by about a hundred, kate, that's how much he thinks he loves you. no, this is mount everest. you should flip on the discovery channel from time to time. but i guess you can't now, being dead and all. rehabilitated? well, now let me see. you know, i don't have any idea what that means. mister wayne, if you don't want to tell me exactly what you're doing, when i'm asked, i don't have to lie. but don't think of me as an idiot. rehabilitated? well, now let me see. you know, i don't have any idea what that means. cities fall but they are rebuilt. heroes die but they are remembered. no, this is mount everest. you should flip on the discovery channel from time to time. but i guess you can't now, being dead and all."},
        {paragraph: "I did the same thing to gandhi, he didn't eat for three weeks. bruce... i'm god. cities fall but they are rebuilt. heroes die but they are remembered. i once heard a wise man say there are no perfect men. only perfect intentions. cities fall but they are rebuilt. heroes die but they are remembered. boxing is about respect. getting it for yourself, and taking it away from the other guy. well, what is it today? more spelunking? let me tell you something my friend. hope is a dangerous thing. hope can drive a man insane. bruce... i'm god. well, what is it today? more spelunking? it only took me six days. same time it took the lord to make the world. i did the same thing to gandhi, he didn't eat for three weeks."},
        {paragraph: "Let me tell you something my friend. hope is a dangerous thing. hope can drive a man insane. boxing is about respect. getting it for yourself, and taking it away from the other guy. mister wayne, if you don't want to tell me exactly what you're doing, when i'm asked, i don't have to lie. but don't think of me as an idiot. you measure yourself by the people who measure themselves by you. circumstances have taught me that a man's ethics are the only possessions he will take beyond the grave. circumstances have taught me that a man's ethics are the only possessions he will take beyond the grave. you measure yourself by the people who measure themselves by you. you measure yourself by the people who measure themselves by you. that tall drink of water with the silver spoon up his ass. i once heard a wise man say there are no perfect men. only perfect intentions. mister wayne, if you don't want to tell me exactly what you're doing, when i'm asked, i don't have to lie. but don't think of me as an idiot. boxing is about respect. getting it for yourself, and taking it away from the other guy."},
        {paragraph: "That tall drink of water with the silver spoon up his ass. well, what is it today? more spelunking? i now issue a new commandment: thou shalt do the dance. let me tell you something my friend. hope is a dangerous thing. hope can drive a man insane. i did the same thing to gandhi, he didn't eat for three weeks. the man likes to play chess; let's get him some rocks. i now issue a new commandment: thou shalt do the dance. i now issue a new commandment: thou shalt do the dance. multiply your anger by about a hundred, kate, that's how much he thinks he loves you. i don't think they tried to market it to the billionaire, spelunking, base-jumping crowd. that tall drink of water with the silver spoon up his ass. it only took me six days. same time it took the lord to make the world. "}
      ]
    },
    movies: [],
    moviesDirectives: {
      cover: {
        src: function() {
          return this.cover;
        }
      },
      niceTitle: {
        href: function() {
          return "#movies/" + this.niceTitle;
        },
        text: function() {
          return '';
        }
      }
    },
    getData: function(data) {
      // this now points at movieApp.content instead of Window
      this.movies = JSON.parse(data);
      // adds average rating to database:
      // for each movie:
      _.each(this.movies, function(value, key) {
        var score = [];
        // maps all reviews:
        _.map(value['reviews'], function(num, key) {
          // saves every rating to the score array:
          score[key] = num['score'];
        });
        // returns average to movie.object:
        value['average-rating'] = _.reduce(score, function(memo, num) { return memo + num / score.length; }, 0);
        value['niceTitle'] = movieApp.utils.friendlyURL(value['title']);
      });
      localStorage["movies"] = JSON.stringify(this.movies);
      movieApp.utils.updateMovies(this.movies);
      movieApp.utils.preloader(false);
    }
  }

  movieApp.sections = {
    init: function() {
      this.about();
      this.movies();
    },
    about: function() {
      var aboutSection = document.querySelector("[data-route='about']");
      Transparency.render(aboutSection, movieApp.content.about);
    },
    movies: function() {
      if (localStorage["movies"]) {
        movieApp.content.movies = JSON.parse(localStorage["movies"]);
        movieApp.utils.updateMovies(movieApp.content.movies);
        movieApp.utils.preloader(false);
      } else {
        // Use .bind(object) to bind the scope to the desired object
        movieApp.utils.XHR('GET', 'static/js/data.js', movieApp.content.getData.bind(movieApp.content));
      }
    },
    movie: function(title) {
      var dataDetails = document.getElementById('data-details');
      var result = _.where(movieApp.content.movies, {niceTitle: title});
      //rating:
      var ratinghtml = document.querySelector(".rating span");
      var rating = _.where(movieApp.content.movies, {niceTitle: title});
      rating = rating[0]['average-rating'] * 8;
      console.log(rating);
      ratinghtml.style.width = rating + 'px';

      Transparency.render(dataDetails, result, movieApp.content.moviesDirectives);

    },
    toggle: function(section) {
      var active = document.querySelector("[data-route='"+ section +"']");
      if (document.querySelector('.active') !== null) {
        document.querySelector('.active').classList.remove('active');
      }
      active.classList.add('active');
    }
  }

  movieApp.utils = {
    XHR: function(type, url, success, data) {
      var req = new XMLHttpRequest;
      req.open(type, url, true);

      req.setRequestHeader('Content-type','application/json');

      type === 'POST' ? req.send(data) : req.send(null);

      req.onreadystatechange = function() {
        if (req.readyState === 4) {
          if (req.status === 200 || req.status === 201) {
            success(req.responseText);
          }
        }
      }
    },
    moviesFilter: function(genre) {
      movieApp.controller.filterActive = true;
      movieApp.controller.filteredList = _(movieApp.content.movies).filter(function(x) { return _(x.genres).any(function(y) {return y === genre;});});
      this.updateMovies(movieApp.controller.filteredList);
    },
    search: function(term) {
      if (term !== '') {
        movieApp.controller.searchActive = true;
        term = this.captializeEachWord(term);
        var result = _.where(movieApp.content.movies, {title: term});
        this.updateMovies(result);
      } else {
        movieApp.controller.searchActive = false;

        if (movieApp.controller.filterActive) {
          this.updateMovies(movieApp.controller.filteredList);
        } else {
          this.updateMovies(movieApp.content.movies);
        }
      }
    },
    sort: function() {
      var result = _.sortBy(movieApp.content.movies, function(o) { return o.title; });
      this.updateMovies(result);
    },
    group: function() {
      var result = _.sortBy(movieApp.content.movies, function(o) { return o.genres; });
      this.updateMovies(result);
    },
    captializeEachWord: function(str) {
      return str.replace(/\w\S*/g, function(txt) {return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    },
    preloader: function(str) {
      var loader = document.querySelector(".loader");

      if (str == false) {
        loader.classList.add('hidden');
      } else {
        loader.classList.remove('hidden');
      }
    },
    friendlyURL: function(text) {
      return text.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
    },
    updateMovies: function(data) {
      var dataMovie = document.getElementById('data-movie');
      Transparency.render(dataMovie, data, movieApp.content.moviesDirectives);
    }
  }

  movieApp.controller.init();

})();