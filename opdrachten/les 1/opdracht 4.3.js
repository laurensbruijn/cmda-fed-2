/*

Closure is het aanroepen van variabelen in andere scopes. Bijvoorbeeld:

*/

function person(name) {
  this.name = name;

  this.leeftijd = function () {
    var jaar = '1994';
  }
}

var bob = new person('Bob');

console.log(bob.name);